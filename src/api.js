const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid, v4 } = require("uuid");
const { json } = require("express");
const { MongoClient } = require("mongodb");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));
  
  const users = mongoClient.db("pingr").collection("users");

  const validateRequest = (req, res, next) => {
    let emptyField = "";
    
    if(!req.body.name) emptyField = "name" 
    else if (!req.body.email) emptyField = "email"
    else if (!req.body.password) emptyField = "password"
    else if (!req.body.passwordConfirmation) emptyField = "passwordConfirmation"
    
    if (emptyField == "") next();
    else {
      res.status(400).json({
        error: `Request body had missing field ${emptyField}`
      })
    }
  }

  const validateFormat = async (req, res, next) => {

    // eslint-disable-next-line no-useless-escape
    const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    let malformedField = "";
    if (req.body.password.length < 8 || req.body.password.length > 32) malformedField = "password";
    else if (req.body.name.length == 0) malformedField = "name";
    else if (req.body.email.lenght == 0 || !req.body.email.match(emailRegex) || await users.findOne({email: req.body.email})) {
      malformedField = "email";
    }
    else if (req.body.passwordConfirmation.length < 8 || req.body.passwordConfirmation.length > 32) malformedField = "passwordConfirmation";


    if (malformedField == "") next();
    else {
      res.status(400).json({
        error: `Request body had malformed field ${malformedField}`
      })
    }
  }

  const validatePassword = (req, res, next) => {
    if (req.body.password === req.body.passwordConfirmation) next();
    else res.status(422).json({error: "Password confirmation did not match"})
  }

  const checkTokenIsPresent = (req, res, next) => {
    if (req.get("Authentication")) next();
    else res.status(401).json({error: "Access Token not found"});
  }

  const validateToken = (req, res, next) => {
    const token = req.get("Authentication").split(" ")[1];


    const decoded = jwt.decode(token);
    if (decoded.id === req.params.uuid) next();
    else res.status(403).json({error: "Access Token did not match User ID"});
  }


  const validations = [validateRequest, validateFormat, validatePassword];


  const tokenValidations = [checkTokenIsPresent, validateToken];


  api.get("/", (req, res) => { 
    res.json("Hello, World!")
  });

  

  api.post("/users", validations, (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;

    const userId = v4();

    const userCreatedEvent = {
      eventType: "UserCreated",
      entityId: userId,
      entityAggregate: {
        name: name,
        email: email,
        password: password
      }
    };

    stanConn.publish("users", JSON.stringify(userCreatedEvent));

    res.status(201).json({
      user: {
        id: userId,
        name: name,
        email: email
      }

    });
  });


  api.delete("/users/:uuid/", tokenValidations, (req, res) => {
    const id = req.params.uuid;

    const userDeletedEvent = {
      eventType: "UserDeleted",
      entityId: id,
      entityAggregate: {}
    }
    
    stanConn.publish("users",  JSON.stringify(userDeletedEvent));

    res.status(200).json({id: id});
  });

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */
  return api;
}
