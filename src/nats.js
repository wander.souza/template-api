const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const database = mongoClient.db("pingr");

    const opts = conn.subscriptionOptions().setStartWithLastReceived();

    const subscription = conn.subscribe('users', opts);
  
    subscription.on('message', (msg) => {
      const event = JSON.parse(msg.getData());

      switch(event.eventType) {
        case "UserCreated": {
          const user = event.entityAggregate;
          user.passwordConfirmation = user.password;
          user._id = event.entityId;

          database.collection("users").insertOne(user);
          break;
        }
        case "UserDeleted": {
          const id = event.entityId;
          database.collection("users").deleteOne({_id: id});
          break;
        }

        default: 
          console.log("Evento não esperado")
      }
      
    });

    /* ******************* */ 
    /* YOUR CODE GOES HERE */
    /* ******************* */
  });

  return conn;
};
